import React from 'react'
import {StyleSheet, View, Text} from 'react-native'

function SignInScreen(){
    return(
        <View style={styles.container}>
            <Text style={styles.text} >SignIng Screen</Text>
            
        </View>
    )
}
const styles = StyleSheet.create({
    container:{
        flex:1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#ebebebeb',
    },
    text: {
        color: '#010101',
        fontSize: 24,
        fontWeight: 'bold'
    }
})

export default SignInScreen;